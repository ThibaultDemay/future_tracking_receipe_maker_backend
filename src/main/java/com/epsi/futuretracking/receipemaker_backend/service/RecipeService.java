package com.epsi.futuretracking.receipemaker_backend.service;

import com.epsi.futuretracking.receipemaker_backend.model.Recipe;
import com.epsi.futuretracking.receipemaker_backend.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    public Recipe findRecipeById(Integer id){
        return recipeRepository.findById(id);
    }

    public List<Recipe> findAll(){
        return recipeRepository.findAll();
    }
}
