package com.epsi.futuretracking.receipemaker_backend.helper;


import com.epsi.futuretracking.receipemaker_backend.exception.AlreadyExistsException;
import com.epsi.futuretracking.receipemaker_backend.model.Recipe;
import com.epsi.futuretracking.receipemaker_backend.repository.RecipeRepository;

public class DataBaseInserter {
    private RecipeRepository recipeRepository = new RecipeRepository();

    public DataBaseInserter() {

    }

    public void fillDataBase() throws AlreadyExistsException {
        //region CREATION DES RECETTES

        recipeRepository.insert(new Recipe("Salade", "contentSalade", "pictureSalade"));
        recipeRepository.insert(new Recipe("Pates carbo", "contentPC", "picturePC"));



//        //endregion


    }
}
