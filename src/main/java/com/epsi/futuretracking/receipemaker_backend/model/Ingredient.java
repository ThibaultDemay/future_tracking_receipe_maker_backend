package com.epsi.futuretracking.receipemaker_backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Ingredient {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ingredient")
    private List<RecipeIngredient> recipeIngredientList;

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
