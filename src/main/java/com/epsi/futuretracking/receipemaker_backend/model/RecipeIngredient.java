package com.epsi.futuretracking.receipemaker_backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "recipe_ingredient")
public class RecipeIngredient {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private int quantity;
    @Column
    private String unit;

    public RecipeIngredient(int id, Ingredient ingredient, Recipe recipe, int quantity, String unit) {
        this.id = id;
        this.recipe = recipe;
        this.ingredient = ingredient;
        this.quantity = quantity;
        this.unit = unit;
    }

    @ManyToOne
    private Recipe recipe;

    @ManyToOne
    private Ingredient ingredient;

    public RecipeIngredient() {
    }


}
