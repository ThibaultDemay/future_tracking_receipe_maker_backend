package com.epsi.futuretracking.receipemaker_backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;
    @Column
    private String content;
    @Column
    private String picture;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "recipe")
    private List<RecipeIngredient> recipeIngredientList;

    public Recipe(String name, String content, String picture, List<RecipeIngredient> recipeIngredientList) {
        this.name = name;
        this.content = content;
        this.picture = picture;
        this.recipeIngredientList = recipeIngredientList;
    }

    public Recipe() {

    }

    public Recipe(String name, String content, String picture) {
        this.name = name;
        this.content = content;
        this.picture = picture;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
