package com.epsi.futuretracking.receipemaker_backend.controller;

import com.epsi.futuretracking.receipemaker_backend.model.Recipe;
import com.epsi.futuretracking.receipemaker_backend.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping(value = "/recipe", produces = {APPLICATION_JSON_VALUE})
public class RecipeController {

    static Logger logger = Logger.getLogger(RecipeController.class);

    @Autowired
    private RecipeService recipeService;

    @GetMapping(value="/{id}")
    public Recipe getBook(@PathVariable int id) {

        logger.info(String.format("Recipe n° %d has been asked and returned.", id));
        return recipeService.findRecipeById(id);
    }

    @GetMapping(value="")
    public List<Recipe> getAllBooks() {
        logger.info(String.format("All Recipes have been asked and returned."));
        return recipeService.findAll();
    }

}
