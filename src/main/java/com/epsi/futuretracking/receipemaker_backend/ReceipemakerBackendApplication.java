package com.epsi.futuretracking.receipemaker_backend;

import com.epsi.futuretracking.receipemaker_backend.exception.AlreadyExistsException;
import com.epsi.futuretracking.receipemaker_backend.helper.DataBaseInserter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReceipemakerBackendApplication {

	public static void main(String[] args) throws AlreadyExistsException {

		SpringApplication.run(ReceipemakerBackendApplication.class, args);
		DataBaseInserter dbInserter = new DataBaseInserter();
		dbInserter.fillDataBase();
	}

}
