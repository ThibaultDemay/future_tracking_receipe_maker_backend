package com.epsi.futuretracking.receipemaker_backend.repository;


import com.epsi.futuretracking.receipemaker_backend.helper.DataBaseHelper;

import javax.persistence.EntityManager;

public class GenericRepository {

    private EntityManager entityManager;

    public GenericRepository() {
        this.entityManager = DataBaseHelper.createEntityManager();
    }

    protected EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            entityManager = DataBaseHelper.createEntityManager();
        }
        return entityManager;
    }

}
