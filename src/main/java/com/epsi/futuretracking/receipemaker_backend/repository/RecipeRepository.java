package com.epsi.futuretracking.receipemaker_backend.repository;

import com.epsi.futuretracking.receipemaker_backend.exception.AlreadyExistsException;
import com.epsi.futuretracking.receipemaker_backend.helper.DataBaseHelper;
import com.epsi.futuretracking.receipemaker_backend.model.Recipe;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

@Component
public class RecipeRepository extends GenericRepository {

    public Recipe insert(Recipe Recipe) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        List<Recipe> existingRecipes = findByNameAndFirstName(Recipe.getName());
        if (!existingRecipes.isEmpty()) {
            throw new AlreadyExistsException("The Recipe named " +Recipe.getName() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(Recipe);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return Recipe;
    }


    public Recipe update(Recipe Recipe) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(Recipe);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return Recipe;
    }



    /**
     * Finds all Recipes.
     * @return A list containing all the Recipes.
     */
    public List<Recipe> findAll() {
        return getEntityManager().createQuery("from Recipe order by id", Recipe.class).getResultList();
    }


    /**
     * Finds a Recipe by its id.
     * @return The matching Recipe, otherwise null.
     * @throws SQLException
     */
    public Recipe findById(int id) {
        return getEntityManager().find(Recipe.class, id);
    }

    /**
     * Finds a Recipe by its name.
     * @return The matching Recipe, otherwise null.
     * @throws SQLException
     */
    public List<Recipe> findByNameAndFirstName(String name) {
        TypedQuery<Recipe> query = getEntityManager().createQuery("select r from Recipe r where r.name = :name " +
                "order by r.name", Recipe.class);
        query.setParameter("name", name);
        return query.getResultList();
    }

}
