package com.epsi.futuretracking.receipemaker_backend.repository;

import com.epsi.futuretracking.receipemaker_backend.exception.AlreadyExistsException;
import com.epsi.futuretracking.receipemaker_backend.helper.DataBaseHelper;
import com.epsi.futuretracking.receipemaker_backend.model.Ingredient;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

@Component
public class IngredientRepository extends GenericRepository {

    public Ingredient insert(Ingredient Ingredient) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        List<Ingredient> existingIngredients = findByNameAndFirstName(Ingredient.getName());
        if (!existingIngredients.isEmpty()) {
            throw new AlreadyExistsException("The Ingredient named " +Ingredient.getName() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(Ingredient);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return Ingredient;
    }


    public Ingredient update(Ingredient Ingredient) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(Ingredient);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return Ingredient;
    }



    /**
     * Finds all Ingredients.
     * @return A list containing all the Ingredients.
     */
    public List<Ingredient> findAll() {
        return getEntityManager().createQuery("from Ingredient order by id", Ingredient.class).getResultList();
    }


    /**
     * Finds a Ingredient by its id.
     * @return The matching Ingredient, otherwise null.
     * @throws SQLException
     */
    public Ingredient findById(int id) {
        return getEntityManager().find(Ingredient.class, id);
    }

    /**
     * Finds a Ingredient by its name.
     * @return The matching Ingredient, otherwise null.
     * @throws SQLException
     */
    public List<Ingredient> findByNameAndFirstName(String name) {
        TypedQuery<Ingredient> query = getEntityManager().createQuery("select r from Ingredient r where r.name = :name " +
                "order by r.name", Ingredient.class);
        query.setParameter("name", name);
        return query.getResultList();
    }

}
